{ pkgs, ... }:
let
  inherit (import ./ssh-keys.nix pkgs)
    cache1Pub cache1Priv cache2Pub cache2Priv;
in {
  nodes = {
    builder1 = { pkgs, ... }: {
      imports = [ (import ./config_builder.nix { cachePub = ./builder1-public; cachePriv = ./builder1-private; }) ];
    };

    builder2 = { pkgs, ... }: {
      # imports = [ ./config_builder.nix ];
      imports = [ (import ./config_builder.nix { cachePub = ./builder2-public; cachePriv = ./builder2-private;}) ];
    };
    
    # laptop1 = {pkgs, ...}: {
    #   imports = [ (import ./config_laptop.nix "builder1") ];
    # };

    # laptop2 = {pkgs, ...}: {
    #   imports = [ (import ./config_laptop.nix "builder2") ];
    # };

  };
  testScript = ''
    foo.succeed("true")
  '';
}
