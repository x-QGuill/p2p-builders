{ pkgs, ... }:
let
  inherit (import ./ssh-keys.nix pkgs)
    snakeOilPrivateKey snakeOilPublicKey cachePub cachePriv;


in
{
  networking.firewall.enable = false;
  services.sshd.enable = true;
  services.openssh = {
    enable = true;
    ports = [ 22 ];
    permitRootLogin = "yes";
    authorizedKeysFiles = [ "${snakeOilPublicKey}" ];
  };
  users.users.root.password = "nixos";
  environment.systemPackages = [ pkgs.git pkgs.htop pkgs.nix-serve];
}
