{cachePriv, cachePub }:
{ pkgs, ... }:
let
  inherit (import ./ssh-keys.nix pkgs)
    snakeOilPrivateKey snakeOilPublicKey cache1Priv cache1Pub;
  sshKeysConf = ''
    cp ${snakeOilPrivateKey} /root/.ssh/id_rsa
    chmod 600 /root/.ssh/id_rsa
    cp ${snakeOilPublicKey} /root/.ssh/id_rsa.pub
    cat ${snakeOilPublicKey} >> /root/.ssh/authorized_keys
  '';

in
{
  imports = [ ./common_config.nix ];
  boot.postBootCommands = ''
    ${sshKeysConf}
    cat ${cachePriv} > /root/cache-priv-key.pem
    cat ${cachePub} > /root/cache-pub-key.pem
    echo "secret-key-files = /root/cache-priv-key.pem" >> /etc/nix/nix.conf
    # systemctl restart nix-daemon
    nix sign-paths --all -k /root/cache-priv-key.pem
  '';
  # services.nix-serve = {
  #   enable = true;
  #   port = 8080;
  # };
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';
  # nix.package = pkgs.nixVersions.nix_2_3;
  
   services.peerix = {
    enable = true;
    openFirewall = true; # UDP/12304
    # privateKeyFile = ../secrets/peerix-private;
    # publicKeyFile = ../secrets/peerix-public;
    privateKeyFile = cachePriv;
    publicKeyFile = cachePub;
    user = "peerix";
    group = "peerix";
    disableBroadcast = true;
    extraHosts = [ "builder1" "builder2" ];
  };
  users.users.peerix = {
    isSystemUser = true;
    group = "peerix";
  };
  users.groups.peerix = { };

  # services.peerix = {
  #   enable = true;
  #   package = pkgs.peerix;
  #   openFirewall = true; # UDP/12304
  #   publicKey = "builder1:2FQ2F9AftnTJLg3lEFFvDQb0s7dzrzvN1zDaXcqFqoo= builder2:3ybAWf+NcW05iEEvdEYWw+DUw67HxS0x7HCUrTqtisg=";
  #   # example # publicKey = "peerix-laptop:1ZjzxYFhzeRMni4CyK2uKHjgo6xy0=";
  # };
}
