{
  description = "nixos-compose - basic setup";

  inputs = {
    # nixpkgs.url = "github:NixOS/nixpkgs/22.11";
    nixpkgs.url = "github:NixOS/nixpkgs/22.05";
    # nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose.git";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose.git?rev=099791a24bcc4a75d683a7c99a06020e9b2fe206";
    peerix.url = "github:VolodiaPG/peerix";
  };

  outputs = { self, nixpkgs, nxc, peerix }:
    let
      system = "x86_64-linux";
      extraConfigurations = [
        # { nixpkgs.overlays = [ peerix.overlay ]; }
        peerix.nixosModules.peerix
      ];
    in
    {
      packages.${system} = nxc.lib.compose {
        inherit nixpkgs system extraConfigurations;
        overlays = [ peerix.overlay ];
        composition = ./composition.nix;
      };

      devShell.${system} = nxc.devShells.${system}.nxcShellFull;
    };
}
