builderName:
{ pkgs, ... }:
let

  inherit (import ./ssh-keys.nix pkgs)
    snakeOilPrivateKey snakeOilPublicKey cachePub cachePriv;
  sshKeysConf = ''
    cp ${snakeOilPrivateKey} /root/.ssh/id_rsa
    chmod 600 /root/.ssh/id_rsa
    cp ${snakeOilPublicKey} /root/.ssh/id_rsa.pub
    cat ${snakeOilPublicKey} >> /root/.ssh/authorized_keys
  '';
in
{
  imports = [ ./common_config.nix ];
  boot.postBootCommands = ''
    ${sshKeysConf}
  '';
  # nix.binaryCaches = [ "http://${builderName}:8080/" ];

  nix.buildMachines = [{
    hostName = "${builderName}";
    system = "x86_64-linux";
    maxJobs = 8;
    speedFactor = 2;
    supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
    mandatoryFeatures = [ ];
  }];
  nix.distributedBuilds = true;
  # optional, useful when the builder has a faster internet connection than yours
  nix.extraOptions = ''
    builders-use-substitutes = true
    experimental-features = nix-command flakes
  '';
  nix.settings.require-sigs = false;
  nix.settings.trusted-public-keys = [ "${cachePub}" ];
  nix.settings.substituters = [ "http://${builderName}:8080/" "ssh-ng://${builderName}" "https://cache.nixos.org/" ];
}
